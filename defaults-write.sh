#!/bin/bash

set_dock_preferences() {
    # Dock を自動的に隠す
    defaults write com.apple.dock autohide -bool true

    # Wipe all app icons from the Dock （Dock に標準で入っている全てのアプリを消す、Finder とごみ箱は消えない）
    defaults write com.apple.dock persistent-apps -array

    # Set the icon size （アイコンサイズの設定）
    defaults write com.apple.dock tilesize -int 32

    # Magnificate the Dock （Dock の拡大機能を入にする）
    #defaults write com.apple.dock magnification -bool true

    # 隠したアプリ(Command+H)を半透明に
    defaults write com.apple.Dock showhidden -boolean true
}

set_finder_preferences() {
    # Set `${HOME}` as the default location for new Finder windows
    # 新しいウィンドウでデフォルトでホームフォルダを開く
    defaults write com.apple.finder NewWindowTarget -string "PfDe"
    defaults write com.apple.finder NewWindowTargetPath -string "file://${HOME}/"

    # Show the ~/Library directory （ライブラリディレクトリを表示、デフォルトは非表示）
    chflags nohidden ~/Library

    # Show the hidden files （不可視ファイルを表示）
    defaults write com.apple.finder AppleShowAllFiles YES

    # デフォルトはカラムビュー表示
    # icnv: アイコンビュー Nlsv: リストビュー  clmv: カラムビュー Flwv: カバーフロービュー
    defaults write com.apple.finder FXPreferredViewStyle Nlsv

    # デスクトップのアイコンサイズ
    defaults write com.apple.finder DesktopViewOptions -dict IconSize -integer 32

    # 保存ダイアログを詳細表示で開く
    defaults write -g NSNavPanelExpandedStateForSaveMode -bool true

    # 印刷ダイアログを詳細表示で開く
    defaults write -g PMPrintingExpandedStateForPrint -bool true
}

set_quicklook_preferences() {
    # Quick Look上でテキストの選択を可能に
    defaults write com.apple.finder QLEnableTextSelection -bool true

    # 他のウィンドウに移った時にQuick Lookを非表示する
    defaults write com.apple.finder QLHidePanelOnDeactivate -bool true
}

set_dashboard_preferences() {
    # dashboard無効化
    defaults write com.apple.dashboard mcx-disabled -bool YES
}

set_system_preferences() {
    # ネットワークフォルダに.DS_Storeを作らない
    defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true

    # Menu bar: show battery percentage
    defaults write com.apple.menuextra.battery ShowPercent -string "YES"
}

set_keyboard_preferences() {
    # Keyboard キーのリピート # normal minimum is 2 (30 ms)
    defaults write NSGlobalDomain KeyRepeat -int 2

    # リピート入力認識までの時間 # normal minimum is 15 (225 ms)
    defaults write NSGlobalDomain InitialKeyRepeat -int 15

    # Enable full keyboard access for all controls
    # Tabで全てのコントロールにフォーカスできるようになる
    defaults write NSGlobalDomain AppleKeyboardUIMode -int 3
}

set_mouse_preferences() {
    # スクロール方向をナチュラルにをOFF
    defaults write NSGlobalDomain com.apple.swipescrolldirection -bool false

    # 副ボタンのクリックを有効(右側をクリック)
    defaults write com.apple.driver.AppleBluetoothMultitouch.mouse MouseButtonMode -string "TwoButton"
    defaults write com.apple.driver.AppleBluetoothMultitouch.mouse "save.MouseButtonMode.v1" -int 1

    # スワイプを1本指で実行する
    defaults write com.apple.driver.AppleBluetoothMultitouch.mouse MouseHorizontalSwipe -int 1

    # Enable `Tap to click` (タップでクリックを有効にする)
    #defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
}

set_trackpad_preferences() {
    # Enable `Tap to click` (タップでクリックを有効にする)
    #defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
    #defaults write NSGlobalDomain com.apple.mouse.tapBehavior -int 1

    # Map bottom right Trackpad corner to right-click (右下をクリックで、副クリックに割り当てる)
    defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadCornerSecondaryClick -int 2
    defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadRightClick -bool true
    defaults -currentHost write NSGlobalDomain com.apple.trackpad.trackpadCornerClickBehavior -int 1
    defaults -currentHost write NSGlobalDomain com.apple.trackpad.enableSecondaryClick -bool true

    # 画面のズーム
    # Use scroll gesture with the Ctrl (^) modifier key to zoom
    defaults write com.apple.universalaccess closeViewScrollWheelToggle -bool true
    defaults write com.apple.universalaccess HIDScrollZoomModifierMask -int 262144
    # Follow the keyboard focus while zoomed in
    defaults write com.apple.universalaccess closeViewZoomFollowsFocus -bool true
}

set_screen_capture_preferences() {
    # Save screenshots as PNGs （スクリーンショット保存形式をPNGにする）
    defaults write com.apple.screencapture type -string "png"

    # キャプチャの保存場所を変更
    defaults write com.apple.screencapture location ~/Downloads

    # キャプチャのプレフィックスを変更
    # defaults write com.apple.screencapture name "SS_"

    # キャプチャに影を付けない
    defaults write com.apple.screencapture disable-shadow -boolean true

}

set_safari_preferences() {
    # Enable the `Develop` menu and the `Web Inspector` （開発メニューを表示）
    defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2DeveloperExtrasEnabled -bool true
    defaults write com.apple.Safari IncludeDevelopMenu -bool true
    defaults write com.apple.Safari WebKitDeveloperExtrasEnabledPreferenceKey -bool true

    # Enable `Debug` menu （メニュー → デバッグを表示）
    defaults write com.apple.Safari IncludeInternalDebugMenu -bool true

    # Add a context menu item for showing the `Web Inspector` in web views
    # コンテキストメニューにWebインスペクタを追加
    defaults write NSGlobalDomain WebKitDeveloperExtras -bool true
}

osascript -e 'tell application "System Preferences" to quit'

set_dock_preferences
set_finder_preferences
set_quicklook_preferences
set_dashboard_preferences
set_system_preferences
set_keyboard_preferences
set_mouse_preferences
set_trackpad_preferences
set_screen_capture_preferences
set_safari_preferences

apps=(cfprefsd
      Dock
      Finder
      SystemUIServer
      Safari)

for app in ${apps[@]}; do
    killall $app
done
