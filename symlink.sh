#!/bin/bash

pushd ~
if [ ! -e .ghq ]; then ln -si code/git .ghq; fi
popd

pushd ~/code
ln -si git/bitbucket.org/tobetchi/codegym
ln -si git/bitbucket.org/tobetchi/lib
ln -si git/bitbucket.org/tobetchi/snippets
popd

pushd ~
ln -si code/git/bitbucket.org/tobetchi/dotfiles/.hammerspoon
ln -si code/git/bitbucket.org/tobetchi/dotfiles/.screenrc
popd
