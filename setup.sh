#!/bin/bash
set -x
REPO_DIR=${REPO_DIR:-~/setup_mac}
set +x

ask() {
  printf "%s [y/n] " "$*"
  local answer
  read -r answer

  case $answer in
    "yes" ) return 0 ;;
    "y" )   return 0 ;;
    * )     return 1 ;;
  esac
}

set -e

if ask 'Install Xcode?'; then
    xcode-select --install
    sudo xcodebuild -license accept
fi

if ask 'Install Homebrew?'; then
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

    brew doctor
fi

if ask 'Install homebrew mas?'; then
    brew install mas
fi

if ask 'Execute brew bundle?'; then
    pushd "$REPO_DIR"
    brew bundle
    popd
fi

if ask 'Set defaults write settings?'; then
    . $REPO_DIR/defaults-write.sh
fi

if ask 'Delete .localized files?'; then
	#find / -type f -name .localized 2> /dev/null
	find / -type f -name .localized -print0 | xargs -0 rm
fi

if ask 'Set symlink?'; then
	. $REPO_DIR/symlink.sh
fi

if ask 'Set computer name?'; then
    printf "New computer name: (%s) >" `scutil --get ComputerName`
    read -r name
    if [ -n "$name" ]; then
        sudo scutil --set ComputerName $name
        sudo scutil --set HostName $name
        sudo scutil --set LocalHostName $name
    fi
fi
